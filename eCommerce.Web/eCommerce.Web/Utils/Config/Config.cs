﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce.Web.Utils.Config
{
    public static class Config
    {
        public static List<Cart> CartList { get; set; } = new List<Cart>();
    }

    public class Cart
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
        public string ImageUrl { get; set; }
    }
}
