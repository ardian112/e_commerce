﻿using eCommerce.Data;
using eCommerce.Service.Interfaces;
using eCommerce.Service.Models;
using eCommerce.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce.Web.Controllers
{

    public class CategoryController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ICategoryService _categoryService;
        private readonly ILogger<CategoryController> _logger;

        public CategoryController(ILogger<CategoryController> logger, AppDbContext context, ICategoryService categoryService)
        {
            _logger = logger;
            _context = context;
            _categoryService = categoryService;
        }


        
        public IActionResult Edit(int? id = null)
        {
            var model = new Category();

            if (id != null)
                model = _context.Categories.Select(x => new Category { Id = x.Id, Name = x.Name , ParentId = x.ParentId }).FirstOrDefault(x => x.Id == (id ?? 0));

            model.ParentCategories = _context.Categories.Where(x=>x.Id!=(id??0)).Select(x => new Category
            {
                Id = x.Id,
                Name = x.Name
                
            }).ToList();

            return View(model);
        }


        public IActionResult Index()
        {
            var model = _categoryService.GetCategories();

            return View(model);
        }

        public IActionResult Delete(int id)
        {
            
            var dbProducts = _context.Products.Where(x => x.CategoryId == id).Any();

            //if not has products for this category
            if (!dbProducts)
            {
                var dbChildCategories = _context.Categories.Where(x => (x.ParentId ?? 0) == id).Any();

                if(!dbChildCategories)
                {
                    var model = _context.Categories.FirstOrDefault(x => x.Id == id);

                    _context.Categories.Remove(model);
                    _context.SaveChanges();
                }
            }

            return RedirectToAction("Index", "Category");
        }

        public IActionResult Save(Category category)
        {
            var model = new eCommerce.Entity.Models.Category();

            if (category.Id>0)
            {
                model = _context.Categories.FirstOrDefault(x => x.Id == category.Id);
                model.Name = category.Name;
                model.ParentId = category.ParentId;
                _context.Categories.Update(model);
            }
            else
            {
                model.Name = category.Name;
                model.ParentId = category.ParentId;
                _context.Categories.Add(model);
            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Category");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
