﻿using eCommerce.Data;
using eCommerce.Service.Interfaces;
using eCommerce.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce.Web.Controllers
{

    public class UserController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IBaseService _baseService;
        private readonly ILogger<UserController> _logger;


        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserController(ILogger<UserController> logger, AppDbContext context, IBaseService baseService,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager
            )
        {
            _logger = logger;
            _context = context;
            _baseService = baseService;

            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        public IActionResult List()
        {
            var model = _context.Users.ToList();
            return View(model);
        }


        public IActionResult Add()
        {
            var model = new Register();
            return View(model);
        }

        public IActionResult Edit(string id)
        {
                var model = _context.Users.Select(x => new Register
                {
                    Id = x.Id,
                    Username = x.UserName,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    BirthDate = (x.BirthDate ?? DateTime.Now),
                    Active=(x.Active??false)
                }).FirstOrDefault(x=>x.Id==id);

            return View(model);
        }


        public IActionResult Profile()
        {

            var user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);
            var id = user.Id;
            var model = _context.Users.Select(x => new Register
            {
                Id = x.Id,
                Username = x.UserName,
                FirstName = x.FirstName,
                LastName = x.LastName,
                BirthDate = (x.BirthDate ?? DateTime.Now),
                Active = x.Active ?? false
            }).FirstOrDefault(x => x.Id == id);

            return View(model);
        }

        


        public IActionResult SaveProfile(Register register)
        {
            var model = _context.Users.FirstOrDefault(x => x.Id == register.Id);
            model.UserName = register.Username;
            model.FirstName = register.FirstName;
            model.LastName = register.LastName;
            model.Active = register.Active;
            model.BirthDate = register.BirthDate;

            _context.Users.Update(model);
            _context.SaveChanges();

            return RedirectToAction("Index", "User");
        }


        [HttpPost]
        public async Task<ActionResult> ChangePassword(string id,string password, string newPassword)
        {
                ApplicationUser user = await _userManager.FindByIdAsync(id);
                if (user != null)
                {
                    var result = await _userManager.ChangePasswordAsync(user, password, newPassword);
                }
            return RedirectToAction("Profile");
        }


        public IActionResult Save(Register register)
        {
            var model = _context.Users.FirstOrDefault(x => x.Id == register.Id);
            model.UserName = register.Username;
            model.FirstName = register.FirstName;
            model.LastName = register.LastName;
            model.Active = register.Active;
            model.BirthDate = register.BirthDate;

            _context.Users.Update(model);
            _context.SaveChanges();

            return RedirectToAction("List", "User");
        }

        public IActionResult Register(Register register)
        {
            try
            {
                ApplicationUser model = new ApplicationUser();

                model.UserName = register.Username;
                model.Email = register.Email;
                model.FirstName = register.FirstName;
                model.LastName = register.LastName;
                model.BirthDate = register.BirthDate;


                IdentityResult result = _userManager.CreateAsync(model, register.Password).Result;

                var deletedRoles = _userManager.GetRolesAsync(model).Result;

                if (deletedRoles.Count > 0)
                    result = _userManager.RemoveFromRolesAsync(model, deletedRoles).Result;


                //if (!string.IsNullOrEmpty(user.RoleId))
                //{
                var newrole = _roleManager.FindByNameAsync("SimpleUser").Result;

                result = _userManager.AddToRoleAsync(model, newrole.Name).Result;
                // }


                return RedirectToAction("List", "User");


            }
            catch (Exception ex)
            {
                return RedirectToAction("List", "User");


            }

        }



        public IActionResult Index()
        {


            //    var products = _context.Products.Select(x => new eCommerce.Service.Models.Product
            //    {
            //        Id = x.Id,
            //        Title = x.Title,
            //        Price = x.Price ?? 0
            //    }).ToList();

            //    var model = new HomeModel
            //    {
            //        Categories = _baseService.GetCategories(),
            //        Products = products
            //    };
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
