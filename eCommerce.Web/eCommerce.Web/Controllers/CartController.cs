﻿using eCommerce.Data;
using eCommerce.Entity.Enumeratinos;
using eCommerce.Entity.Models;
using eCommerce.Service.Interfaces;
using eCommerce.Web.Utils.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace eCommerce.Web.Controllers
{
    public class CartController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ILogger<CartController> _logger;
        private readonly IUserService _userService;

        public CartController(ILogger<CartController> logger, AppDbContext context, IUserService userService)
        {
            _logger = logger;
            _context = context;
            _userService = userService;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {

            foreach (var item in Config.CartList)
            {
                var product = _context.Products.FirstOrDefault(x => x.Id == item.Id);
                if (product != null)
                {
                    if (!string.IsNullOrEmpty(product.ImageUrl))
                        item.ImageUrl = product.ImageUrl;
                }

            }
            return View();
        }

        [HttpPost]
        public IActionResult Checkout(Cart[] cartList)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);

            if (cartList.Length > 0)
            {
                var order = new Order();
                order.UserId = user.Id;
                order.CreatedDate = DateTime.Now;
                order.Status = OrderStatus.Pending;

                order.OrderDetails = cartList.Select(x => new OrderDetail
                {
                    ProductId=x.Id,
                    Quantity=x.Quantity,
                    Price=x.Price
                }).ToList();

                _context.Orders.Add(order);
                _context.SaveChanges();

                Config.CartList.Clear();

                _userService.SendEmail(user);


            }

            return RedirectToAction("Index");
        }
    }
}
