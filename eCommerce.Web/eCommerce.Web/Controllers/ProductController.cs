﻿using eCommerce.Data;
using eCommerce.Service.Interfaces;
using eCommerce.Service.Models;
using eCommerce.Web.Models;
using eCommerce.Web.Utils.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce.Web.Controllers
{

    [AllowAnonymous]
    public class ProductController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IBaseService _baseService;
        private readonly ILogger<ProductController> _logger;

        public ProductController(ILogger<ProductController> logger, AppDbContext context, IBaseService baseService)
        {
            _logger = logger;
            _context = context;
            _baseService = baseService;
        }


        public IActionResult Detail(int id)
        {
            var product = _context.Products.Select(x => new eCommerce.Service.Models.Product
            {
                Id = x.Id,
                Title = x.Title,
                Price = x.Price ?? 0,
                ImageUrl = x.ImageUrl
            }).FirstOrDefault(x => x.Id == id);

            var comments = _context.Comments.Where(x => x.ProductId == id).Select(x => new Comment
            {
                Id = x.Id,
                Description = x.Description,
                CreteDate = x.CreatedDate,
                UserId = x.UserId
            }).ToList();


            foreach (var item in comments)
            {
                var user = _context.Users.FirstOrDefault(x => x.Id == item.UserId);
                item.UserName = user.UserName;
            }

            var model = new Web.Models.ProductModel
            {
                Categories = _baseService.GetCategories(),
                Product = product,
                Comments = comments
            };
            return View(model);
        }

        public IActionResult Save(eCommerce.Entity.Models.Product product, IFormFile file)
        {
            if (product.Id > 0)
                _context.Products.Update(product);
            else
                _context.Products.Add(product);

            _context.SaveChanges();

            if (file != null)
                if (!string.IsNullOrEmpty(file.FileName))
                {
                    var extension = Path.GetExtension(file.FileName);
                    product.ImageUrl = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images/products", product.Id + extension);


                    using (var stream = new FileStream(product.ImageUrl, FileMode.Create))
                    {
                        file.CopyToAsync(stream).Wait();
                    }

                    product.ImageUrl = "images/products/" + product.Id + extension;

                    var dbProduct = _context.Products.FirstOrDefault(x => x.Id == product.Id);
                    dbProduct.ImageUrl = product.ImageUrl;
                    _context.Products.Update(dbProduct);
                    _context.SaveChanges();
                }

            return RedirectToAction("List", "Product");
        }


        public IActionResult AddCart(int id)
        {
            var model = _context.Products.FirstOrDefault(x => x.Id == id);

            var cartItem = Config.CartList.FirstOrDefault(x => x.Id == id);

            if (cartItem == null)
            {
                cartItem = new Cart
                {
                    Id = model.Id,
                    Name = model.Title,
                    Quantity = 1,
                    Price = model.Price ?? 0
                };

                Config.CartList.Add(cartItem);
            }
            else
            {
                foreach (var item in Config.CartList)
                {
                    if (item.Id == id)
                    {
                        item.Quantity += 1;
                        break;
                    }
                }
            }


            return RedirectToAction("Index", "Home");
        }

        public IActionResult Index(int categoryId)
        {
            var products = _context.Products.Where(x => x.CategoryId == categoryId).Select(x => new eCommerce.Service.Models.Product
            {
                Id = x.Id,
                Title = x.Title,
                Price = x.Price ?? 0,
                ImageUrl = x.ImageUrl
            }).ToList();

            var model = new Web.Models.ProductModel
            {
                Categories = _baseService.GetCategories(),
                Products = products
            };
            return View(model);
        }

        public IActionResult List()
        {
            var model = _context.Products.Select(x => new eCommerce.Service.Models.Product
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                ImageUrl = x.ImageUrl,
                Price = x.Price ?? 0
            }).ToList();

            return View(model);
        }



        public IActionResult Edit(int? id = null)
        {
            var product = new eCommerce.Entity.Models.Product();
            if (id != null)
                product = _context.Products.FirstOrDefault(x => x.Id == (id ?? 0));

            var categories = _context.Categories.ToList();


            var model = new ProductEditModel
            {
                Product = product,
                Categories = categories
            };

            return View(model);
        }

        public IActionResult AddComment(int id,string comment)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserName == User.Identity.Name);

            _context.Comments.Add(new eCommerce.Entity.Models.Comment { ProductId = id, Description = comment, UserId = user.Id, CreatedDate = DateTime.Now });

            _context.SaveChanges();

            return RedirectToAction("Detail", new { id = id });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
