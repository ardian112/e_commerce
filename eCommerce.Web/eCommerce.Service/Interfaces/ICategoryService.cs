﻿using eCommerce.Service.Models;
using System.Collections.Generic;

namespace eCommerce.Service.Interfaces
{
    public interface ICategoryService
    {
        List<Category> GetCategories();
    }
}
