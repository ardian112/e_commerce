﻿using eCommerce.Data;
using eCommerce.Service.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Service.Interfaces
{
    public interface IUserService
    {
        void SendEmail(ApplicationUser user);
    }
}
