﻿using eCommerce.Data;
using eCommerce.Service.Interfaces;
using eCommerce.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eCommerce.Service.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly AppDbContext _context;

        public CategoryService(AppDbContext context)
        {
            _context = context;
        }
        public List<Category> GetCategories()
        {
            var categories = _context.Categories.Select(x => new eCommerce.Service.Models.Category
            {
                Id = x.Id,
                Name = x.Name,
                ParentId=x.ParentId
            }).ToList();

            foreach(var item in categories)
            {
                var category = _context.Categories.FirstOrDefault(x => x.Id == (item.ParentId??0));
                if(category!=null)
                {
                    item.ParentCategory = category.Name;
                }
            }

            return categories;
        }
    }
}
