﻿using eCommerce.Data;
using eCommerce.Service.Interfaces;
using eCommerce.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace eCommerce.Service.Services
{
    public class UserService : IUserService
    {
        public void SendEmail(ApplicationUser user)
        {

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress("derstilaardian@gmail.com");
            mail.To.Add(user.Email);
            mail.Subject = "Order Created";
            mail.Body = $"Dear {user.FirstName} {user.LastName}, Your order is Created";

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("derstilaardian@gmail.com", "123456789");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }
    }
}
